import React from 'react';
import ReactDOM from 'react-dom';
// Write your exam JS here

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      profiles: [{name: "Raji Khabbaz", description: "Raji is a Managing partner"}, {name: "Gary Brode", description: "Gary Brode is the managing partner"}]
    };
    this.renderProfile = this.renderProfile.bind(this);
    this.openEditModal = this.openEditModal.bind(this);
    this.closeEditModal = this.closeEditModal.bind(this);
    this.renderModalEditForm = this.renderModalEditForm.bind(this);
    this.updateProfile = this.updateProfile.bind(this);
    this.addManager = this.addManager.bind(this);
  }

  renderProfile() {
    let key = 0;
    return this.state.profiles.map((manager) => {
      return (
        <div className="profile group" key={key++}>
          <div className="profile-image"><img src="../../../public/images/executive-default-image.png"/></div>
          <div className="name-descript">
            <div className="name">{manager.name}</div>
            <div className="description">{manager.description}</div>
          </div>
        </div>
      );
    });
  }

  openEditModal() {
    var modal = document.getElementById('modal');
    modal.style.display = "block";
  }

  closeEditModal() {
    var modal = document.getElementById('modal');
    modal.style.display = "none";
  }

  updateProfile() {
    const names = document.getElementsByClassName("text-name");
    const bios = document.getElementsByClassName("text-bio");
    let tempProfiles = this.state.profiles;
    for (var i = 0; i < names.length; i++) {
      if (i < this.state.profiles.length) {
        tempProfiles[i].name = names[i].value;
        tempProfiles[i].description = bios[i].value;
      } else if (names[i].value.lenght !== 0) {
        tempProfiles[tempProfiles.length] = { name: names[i].value, description: bios[i].value };
      }
    }
    this.setState({profiles: tempProfiles});
    this.closeEditModal();
  }

  addManager() {
    let tempProfiles = this.state.profiles;
    tempProfiles[tempProfiles.length] = { name: "", description: "" };
    this.setState({profiles: tempProfiles});
  }

  renderModalEditForm() {
    let key = 0;
    return (this.state.profiles.map((manager) => {
      return (
                <div className="modal-edit" key={key++}>
                  <div className="modal-edit-header">Manager Information:</div>
                  <div className="modal-name-container">
                    <div className="modal-name">Name</div>
                    <input className="text-name" type="text" key={key} value={manager.name}></input>
                  </div>
                  <div className="modal-bio-container">
                    <div className="modal-bio">Bio</div>
                    <textarea className="text-bio" rows="7" cols="50" >{manager.description}</textarea>
                  </div>
                </div>

        );
      })
    );
  }

render() {
  return(
    <div>
      <div id="modal">
        <div className="modal-content">
          <div className="modal-header">Edit Executive Management
            <button className="modal-x" onClick={this.closeEditModal}>x</button>
          </div>
          {this.renderModalEditForm()}
          <button className="modal-add-manager">Add Another Manager</button>
          <button className="modal-update" onClick={this.updateProfile}>Update</button>
          <button className="modal-cancel" onClick={this.closeEditModal}>Cancel</button>
        </div>
      </div>
      <div className="main">
        <div className="header"><img src="../../../public/images/collapse-icon-down.png"/>
          <div className="title">Excutive Management</div>
          <div className="edit-button" onClick={this.openEditModal}>EDIT</div>
        </div>
        <div className="all-profiles group">{this.renderProfile()}</div>
      </div>
    </div>

  );}
}

export default Main;
