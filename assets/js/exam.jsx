import '../css/normalize'; // Normalize CSS styles across browsers
import '../css/exam'; // Place your exam CSS in this file
import React from 'react';
import ReactDOM from 'react-dom';
import Main from './components/main';

document.addEventListener('DOMContentLoaded', () => {
  const app = document.getElementById('app');
  ReactDOM.render(<Main store={{}} />, app);
});
